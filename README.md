# Simple-Custom-Processor-V2

This is a simple custom processor. 
The custom processor gets a flow file, gets two numbers which are the properties of this processor, sum them and put them in the flow file.
## Integration
You have to put the nar file in ${PATH}/nifi-myprocessor-nar/target/nifi-myprocessor-nar-1.11.4.nar to the lib file of Apache nifi installation.